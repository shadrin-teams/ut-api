let firmRule = require('./firms/firm.rules');
let firmPageRule = require('./firms/firm.page.rules');
let firmServiceRule = require('./firms/firm.service.rules');
let tourRules = require('./tours/tour.rules');
let tourContactsRules = require('./tours/tour.contacts.rules');

module.exports = {
  'Tours': tourRules,
  'Tours_contacts': tourContactsRules,
  'Firms': firmRule,
  'Firm_user_pages': firmPageRule,
  'Firm_user_services': firmServiceRule
};