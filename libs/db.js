let fs = require('fs');
let path = require('path');
let Sequelize = require('sequelize');
let {connectDB} = require('api-shared');
console.log('NODE_ENV', process.env.NODE_ENV);
const env = process.env.NODE_ENV;
let dataBase, userName, password, host;

// if (env === 'production') {
  host = '91.240.84.122';
  dataBase = 'uzbekistan-tours';
  userName = 'ut-admin';
  password = 'xX0oO9cB5ahS4a';
// }

const db = connectDB(dataBase, userName, password, host);

fs.readdirSync('./models').forEach((file) => {
  let model = db.sequelize['import'](path.join('../models', file));
  db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

module.exports = db;
