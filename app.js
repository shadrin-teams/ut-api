let {appBaseInit} = require('api-shared');
let db = require('./libs/db');
let cors = require('cors');
let siteRoutes = require('./routes'),
    apiRoutes = require('./routes/api');

const allowedOrigins = ['http://127.0.0.1:4200', 'http://localhost:4200', 'http://localhost:2000', 'http://uzbekistan-tours.ru:2000', 'http://uzbekistan-tours.ru'];

let app = appBaseInit(db, allowedOrigins) ;

app.use('/', siteRoutes);
app.use('/api', apiRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).send({status: 404, error: '404'});
});

// error handler
app.use(function(err, req, res, next) {
    console.log('______________________500', err);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    // res.send({status: 500, error: err.message});
});

module.exports = app;
