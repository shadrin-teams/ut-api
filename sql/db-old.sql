PGDMP         ,                 w         
   travelblog     10.8 (Ubuntu 10.8-1.pgdg18.04+1)     11.3 (Ubuntu 11.3-1.pgdg18.04+1) >    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16401 
   travelblog    DATABASE     |   CREATE DATABASE travelblog WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE travelblog;
             postgres    false            �           0    0    DATABASE travelblog    ACL     -   GRANT ALL ON DATABASE travelblog TO tbadmin;
                  postgres    false    3024            �            1259    16402    access_token_id_seq    SEQUENCE     |   CREATE SEQUENCE public.access_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.access_token_id_seq;
       public       tbadmin    false            �            1259    16404    Access_tokens    TABLE     L  CREATE TABLE public."Access_tokens" (
    id integer DEFAULT nextval('public.access_token_id_seq'::regclass) NOT NULL,
    user_id integer,
    ip character varying(15) NOT NULL,
    token character varying(255) NOT NULL,
    exp_date integer,
    "createdAt" date,
    "updatedAt" date,
    refresh_token character varying(150)
);
 #   DROP TABLE public."Access_tokens";
       public         tbadmin    false    196            �            1259    16408    countries_id_seq    SEQUENCE     y   CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.countries_id_seq;
       public       tbadmin    false            �            1259    16410 	   Countries    TABLE     �   CREATE TABLE public."Countries" (
    id integer DEFAULT nextval('public.countries_id_seq'::regclass) NOT NULL,
    name character varying(150) NOT NULL,
    "createdAt" date,
    "updatedAt" date
);
    DROP TABLE public."Countries";
       public         tbadmin    false    198            �            1259    16414    tours_id_seq    SEQUENCE     u   CREATE SEQUENCE public.tours_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.tours_id_seq;
       public       tbadmin    false            �            1259    16416    Images    TABLE     �  CREATE TABLE public."Images" (
    id integer DEFAULT nextval('public.tours_id_seq'::regclass) NOT NULL,
    title character varying(255) NOT NULL,
    user_id integer NOT NULL,
    country_id integer,
    city_id integer,
    image character varying(255) NOT NULL,
    slug character varying(255),
    "createdAt" date,
    "updatedAt" date,
    description text,
    public boolean
);
    DROP TABLE public."Images";
       public         tbadmin    false    200            �            1259    16423    rights_blocks_id_seq    SEQUENCE     }   CREATE SEQUENCE public.rights_blocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.rights_blocks_id_seq;
       public       tbadmin    false            �            1259    16425    Rights_blocks    TABLE     �   CREATE TABLE public."Rights_blocks" (
    id integer DEFAULT nextval('public.rights_blocks_id_seq'::regclass) NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255),
    "createdAt" date,
    "updatedAt" date
);
 #   DROP TABLE public."Rights_blocks";
       public         tbadmin    false    202            �            1259    16432    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       tbadmin    false            �            1259    16434    Users    TABLE     �  CREATE TABLE public."Users" (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    last_name character varying(150),
    avatar character varying(150),
    sex integer,
    password character varying(255) NOT NULL,
    salt character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    role_id integer,
    "createdAt" date,
    "updatedAt" date,
    name character varying(150)
);
    DROP TABLE public."Users";
       public         tbadmin    false    204            �            1259    16441    users_rights_id_seq    SEQUENCE     |   CREATE SEQUENCE public.users_rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.users_rights_id_seq;
       public       tbadmin    false            �            1259    16443    Users_rights    TABLE     n  CREATE TABLE public."Users_rights" (
    id integer DEFAULT nextval('public.users_rights_id_seq'::regclass) NOT NULL,
    user_id integer NOT NULL,
    "create" boolean DEFAULT false,
    read boolean DEFAULT false,
    update_action boolean DEFAULT false,
    delete_action boolean DEFAULT false,
    "createdAt" date,
    "updatedAt" date,
    right_id integer
);
 "   DROP TABLE public."Users_rights";
       public         tbadmin    false    206            �            1259    16451    cities_id_seq    SEQUENCE     v   CREATE SEQUENCE public.cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.cities_id_seq;
       public       tbadmin    false            �            1259    16453    files_id_seq    SEQUENCE     u   CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.files_id_seq;
       public       tbadmin    false            �            1259    16455    firm_moderations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.firm_moderations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.firm_moderations_id_seq;
       public       tbadmin    false            �            1259    16457    firm_pages_id_seq    SEQUENCE     z   CREATE SEQUENCE public.firm_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.firm_pages_id_seq;
       public       tbadmin    false            �            1259    16459    firm_services_id_seq    SEQUENCE     }   CREATE SEQUENCE public.firm_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.firm_services_id_seq;
       public       tbadmin    false            �            1259    16461    firm_user_pages_id_seq    SEQUENCE        CREATE SEQUENCE public.firm_user_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.firm_user_pages_id_seq;
       public       tbadmin    false            �            1259    16463    firm_user_services_id_seq    SEQUENCE     �   CREATE SEQUENCE public.firm_user_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.firm_user_services_id_seq;
       public       tbadmin    false            �            1259    16465    firms_id_seq    SEQUENCE     u   CREATE SEQUENCE public.firms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.firms_id_seq;
       public       tbadmin    false            �            1259    16467    messages_id_seq    SEQUENCE     x   CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.messages_id_seq;
       public       tbadmin    false            �            1259    16469    Сities    TABLE     �   CREATE TABLE public."Сities" (
    id integer DEFAULT nextval('public.cities_id_seq'::regclass) NOT NULL,
    name character varying(150) NOT NULL,
    country_id integer NOT NULL,
    "createdAt" date,
    "updatedAt" date
);
    DROP TABLE public."Сities";
       public         tbadmin    false    208            �          0    16404    Access_tokens 
   TABLE DATA               t   COPY public."Access_tokens" (id, user_id, ip, token, exp_date, "createdAt", "updatedAt", refresh_token) FROM stdin;
    public       tbadmin    false    197   �D       �          0    16410 	   Countries 
   TABLE DATA               I   COPY public."Countries" (id, name, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    199   'E       �          0    16416    Images 
   TABLE DATA               �   COPY public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, public) FROM stdin;
    public       tbadmin    false    201   }E       �          0    16425    Rights_blocks 
   TABLE DATA               T   COPY public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    203   �E       �          0    16434    Users 
   TABLE DATA               }   COPY public."Users" (id, last_name, avatar, sex, password, salt, email, role_id, "createdAt", "updatedAt", name) FROM stdin;
    public       tbadmin    false    205   �E       �          0    16443    Users_rights 
   TABLE DATA               �   COPY public."Users_rights" (id, user_id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id) FROM stdin;
    public       tbadmin    false    207   eF       �          0    16469    Сities 
   TABLE DATA               S   COPY public."Сities" (id, name, country_id, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    217   �F       �           0    0    access_token_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.access_token_id_seq', 1, true);
            public       tbadmin    false    196            �           0    0    cities_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.cities_id_seq', 8, true);
            public       tbadmin    false    208            �           0    0    countries_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.countries_id_seq', 3, true);
            public       tbadmin    false    198            �           0    0    files_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.files_id_seq', 1, false);
            public       tbadmin    false    209            �           0    0    firm_moderations_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.firm_moderations_id_seq', 1, false);
            public       tbadmin    false    210            �           0    0    firm_pages_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.firm_pages_id_seq', 1, false);
            public       tbadmin    false    211            �           0    0    firm_services_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.firm_services_id_seq', 1, false);
            public       tbadmin    false    212            �           0    0    firm_user_pages_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.firm_user_pages_id_seq', 1, false);
            public       tbadmin    false    213            �           0    0    firm_user_services_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.firm_user_services_id_seq', 1, false);
            public       tbadmin    false    214            �           0    0    firms_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.firms_id_seq', 7, true);
            public       tbadmin    false    215            �           0    0    messages_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.messages_id_seq', 1, false);
            public       tbadmin    false    216            �           0    0    rights_blocks_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.rights_blocks_id_seq', 2, true);
            public       tbadmin    false    202            �           0    0    tours_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.tours_id_seq', 1, false);
            public       tbadmin    false    200            �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 1, true);
            public       tbadmin    false    204            �           0    0    users_rights_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.users_rights_id_seq', 2, true);
            public       tbadmin    false    206            +           2606    16474    Countries pk_c_id 
   CONSTRAINT     Q   ALTER TABLE ONLY public."Countries"
    ADD CONSTRAINT pk_c_id PRIMARY KEY (id);
 =   ALTER TABLE ONLY public."Countries" DROP CONSTRAINT pk_c_id;
       public         tbadmin    false    199            8           2606    16476    Сities pk_cities_id 
   CONSTRAINT     T   ALTER TABLE ONLY public."Сities"
    ADD CONSTRAINT pk_cities_id PRIMARY KEY (id);
 @   ALTER TABLE ONLY public."Сities" DROP CONSTRAINT pk_cities_id;
       public         tbadmin    false    217            )           2606    16478    Access_tokens pk_id 
   CONSTRAINT     S   ALTER TABLE ONLY public."Access_tokens"
    ADD CONSTRAINT pk_id PRIMARY KEY (id);
 ?   ALTER TABLE ONLY public."Access_tokens" DROP CONSTRAINT pk_id;
       public         tbadmin    false    197            /           2606    16480    Rights_blocks pk_r_b_id 
   CONSTRAINT     W   ALTER TABLE ONLY public."Rights_blocks"
    ADD CONSTRAINT pk_r_b_id PRIMARY KEY (id);
 C   ALTER TABLE ONLY public."Rights_blocks" DROP CONSTRAINT pk_r_b_id;
       public         tbadmin    false    203            -           2606    16482    Images pk_tours_id 
   CONSTRAINT     R   ALTER TABLE ONLY public."Images"
    ADD CONSTRAINT pk_tours_id PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Images" DROP CONSTRAINT pk_tours_id;
       public         tbadmin    false    201            5           2606    16484    Users_rights pk_u_r_id 
   CONSTRAINT     V   ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT pk_u_r_id PRIMARY KEY (id);
 B   ALTER TABLE ONLY public."Users_rights" DROP CONSTRAINT pk_u_r_id;
       public         tbadmin    false    207            1           2606    16486    Users pk_users_id 
   CONSTRAINT     Q   ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT pk_users_id PRIMARY KEY (id);
 =   ALTER TABLE ONLY public."Users" DROP CONSTRAINT pk_users_id;
       public         tbadmin    false    205            6           1259    16487    fki_country_id_fk    INDEX     M   CREATE INDEX fki_country_id_fk ON public."Сities" USING btree (country_id);
 %   DROP INDEX public.fki_country_id_fk;
       public         tbadmin    false    217            2           1259    16488    fki_right_id_fk    INDEX     N   CREATE INDEX fki_right_id_fk ON public."Users_rights" USING btree (right_id);
 #   DROP INDEX public.fki_right_id_fk;
       public         tbadmin    false    207            3           1259    16489    fki_user_id_fk    INDEX     L   CREATE INDEX fki_user_id_fk ON public."Users_rights" USING btree (user_id);
 "   DROP INDEX public.fki_user_id_fk;
       public         tbadmin    false    207            ;           2606    16490    Сities country_id_fk    FK CONSTRAINT        ALTER TABLE ONLY public."Сities"
    ADD CONSTRAINT country_id_fk FOREIGN KEY (country_id) REFERENCES public."Countries"(id);
 A   ALTER TABLE ONLY public."Сities" DROP CONSTRAINT country_id_fk;
       public       tbadmin    false    2859    217    199            9           2606    16495    Users_rights right_id_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT right_id_fk FOREIGN KEY (right_id) REFERENCES public."Rights_blocks"(id);
 D   ALTER TABLE ONLY public."Users_rights" DROP CONSTRAINT right_id_fk;
       public       tbadmin    false    203    207    2863            :           2606    16500    Users_rights user_id_fk    FK CONSTRAINT     z   ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES public."Users"(id);
 C   ALTER TABLE ONLY public."Users_rights" DROP CONSTRAINT user_id_fk;
       public       tbadmin    false    207    205    2865            �   �   x��A�0@������6w,�p�$Қ"��ؠ����)��<$HPhH�����Blz�S�b�����ꗠ��e��8Q��<��֥E���e���}��/�Y_ːgCTևy�늠�FH�
� �h�bU�@���іSJ�L<'�      �   F   x�3估���/l������.l���3����8/,��(���
s^�T��wu"$b���� (�      �      x������ � �      �   3   x�3估���\��L�,�-��".#�Ć;/�K��$b���� �0�      �   x   x�3��HL)�����4�T1JT1�PI.�(w��1uK�
J�uvʏ03LMt���4)NK�	u�������MI)H*��ru4��TIK7��ʰ�LL���3rH�M���K��4��b���� (�"\      �      x�3�4�,��?2�2�3����� �5�      �   y   x�=��	�P ���B�b��c��3T�T�(�j]Ბ)�#��ݑT�Q��e`�Z�R^|C&<Qf��Ke��Ix1�F#ʅ��]���tǅ�`��������9��֏��o��)��O�K)     