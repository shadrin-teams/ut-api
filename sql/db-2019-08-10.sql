PGDMP                 
        w         
   travelblog #   10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)     11.3 (Ubuntu 11.3-1.pgdg18.04+1) p    1           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            2           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            3           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            4           1262    16401 
   travelblog    DATABASE     |   CREATE DATABASE travelblog WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE travelblog;
             postgres    false            5           0    0    DATABASE travelblog    ACL     -   GRANT ALL ON DATABASE travelblog TO tbadmin;
                  postgres    false    3124            �           1247    16554    product_type    TYPE     9   CREATE TYPE public.product_type AS ENUM (
    'image'
);
    DROP TYPE public.product_type;
       public       tbadmin    false            �            1259    16402    access_token_id_seq    SEQUENCE     |   CREATE SEQUENCE public.access_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.access_token_id_seq;
       public       tbadmin    false            �            1259    16404    Access_tokens    TABLE     :  CREATE TABLE public."Access_tokens" (
    id integer DEFAULT nextval('public.access_token_id_seq'::regclass) NOT NULL,
    user_id integer,
    ip character varying(15) NOT NULL,
    token character varying(255) NOT NULL,
    exp_date integer,
    "createdAt" date,
    "updatedAt" date,
    refresh_token text
);
 #   DROP TABLE public."Access_tokens";
       public         tbadmin    false    196            �            1259    16522    categories_id    SEQUENCE     v   CREATE SEQUENCE public.categories_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.categories_id;
       public       tbadmin    false            �            1259    16517 
   Categories    TABLE     	  CREATE TABLE public."Categories" (
    id integer DEFAULT nextval('public.categories_id'::regclass) NOT NULL,
    "createdAt" date NOT NULL,
    "updatedAt" date,
    name character varying(150) NOT NULL,
    "position" integer,
    public boolean DEFAULT false
);
     DROP TABLE public."Categories";
       public         tbadmin    false    220            �            1259    16559    Category_links    TABLE     �   CREATE TABLE public."Category_links" (
    id integer NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    product_id integer NOT NULL,
    category_id integer NOT NULL,
    product_type public.product_type NOT NULL
);
 $   DROP TABLE public."Category_links";
       public         tbadmin    false    674            �            1259    16596    comment_id_seq    SEQUENCE     w   CREATE SEQUENCE public.comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.comment_id_seq;
       public       tbadmin    false            �            1259    16588    Comments    TABLE     S  CREATE TABLE public."Comments" (
    id integer DEFAULT nextval('public.comment_id_seq'::regclass) NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    user_id integer NOT NULL,
    title character varying(150) NOT NULL,
    description text NOT NULL,
    product_id integer NOT NULL,
    product_type public.product_type NOT NULL
);
    DROP TABLE public."Comments";
       public         tbadmin    false    227    674            �            1259    16408    countries_id_seq    SEQUENCE     y   CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.countries_id_seq;
       public       tbadmin    false            �            1259    16410 	   Countries    TABLE     �   CREATE TABLE public."Countries" (
    id integer DEFAULT nextval('public.countries_id_seq'::regclass) NOT NULL,
    name character varying(150) NOT NULL,
    "createdAt" date,
    "updatedAt" date
);
    DROP TABLE public."Countries";
       public         tbadmin    false    198            �            1259    16611    Emotions    TABLE     �   CREATE TABLE public."Emotions" (
    id integer NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    product_id integer NOT NULL,
    product_type public.product_type NOT NULL,
    ip character varying NOT NULL,
    emotion integer NOT NULL
);
    DROP TABLE public."Emotions";
       public         tbadmin    false    674            6           0    0    COLUMN "Emotions".emotion    COMMENT     I   COMMENT ON COLUMN public."Emotions".emotion IS '''like: 1
dislike: 0''';
            public       tbadmin    false    229            �            1259    16453    files_id_seq    SEQUENCE     u   CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.files_id_seq;
       public       tbadmin    false            �            1259    16508    Files    TABLE     �  CREATE TABLE public."Files" (
    id integer DEFAULT nextval('public.files_id_seq'::regclass) NOT NULL,
    folder character varying(150) NOT NULL,
    is_used boolean,
    section character varying(150) NOT NULL,
    item_id integer NOT NULL,
    user_id integer NOT NULL,
    type character varying(50),
    main boolean,
    "order" integer,
    title character varying,
    file character varying(255) NOT NULL,
    "createdAt" date,
    "updatedAt" date
);
    DROP TABLE public."Files";
       public         tbadmin    false    209            �            1259    16530    hash_tag_id_seq    SEQUENCE     x   CREATE SEQUENCE public.hash_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.hash_tag_id_seq;
       public       tbadmin    false            �            1259    16525 	   Hash_tags    TABLE     �   CREATE TABLE public."Hash_tags" (
    id integer DEFAULT nextval('public.hash_tag_id_seq'::regclass) NOT NULL,
    "createdAt" date,
    "updatedAt" date,
    name character varying(150) NOT NULL
);
    DROP TABLE public."Hash_tags";
       public         tbadmin    false    222            �            1259    16414    tours_id_seq    SEQUENCE     u   CREATE SEQUENCE public.tours_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.tours_id_seq;
       public       tbadmin    false            �            1259    16416    Images    TABLE     �  CREATE TABLE public."Images" (
    id integer DEFAULT nextval('public.tours_id_seq'::regclass) NOT NULL,
    title character varying(255) NOT NULL,
    user_id integer NOT NULL,
    country_id integer,
    city_id integer,
    image character varying(255),
    slug character varying(255),
    "createdAt" date,
    "updatedAt" date,
    description text,
    public boolean,
    category_id integer NOT NULL
);
    DROP TABLE public."Images";
       public         tbadmin    false    200            �            1259    16423    rights_blocks_id_seq    SEQUENCE     }   CREATE SEQUENCE public.rights_blocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.rights_blocks_id_seq;
       public       tbadmin    false            �            1259    16425    Rights_blocks    TABLE     �   CREATE TABLE public."Rights_blocks" (
    id integer DEFAULT nextval('public.rights_blocks_id_seq'::regclass) NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255),
    "createdAt" date,
    "updatedAt" date
);
 #   DROP TABLE public."Rights_blocks";
       public         tbadmin    false    202            �            1259    16532    tag_links_id_seq    SEQUENCE     y   CREATE SEQUENCE public.tag_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.tag_links_id_seq;
       public       tbadmin    false            �            1259    16535 	   Tag_links    TABLE       CREATE TABLE public."Tag_links" (
    id integer DEFAULT nextval('public.tag_links_id_seq'::regclass) NOT NULL,
    tag_id integer NOT NULL,
    product_id integer NOT NULL,
    "createdAt" date NOT NULL,
    "updatedAt" date,
    product_type public.product_type
);
    DROP TABLE public."Tag_links";
       public         tbadmin    false    223    674            �            1259    16432    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       tbadmin    false            �            1259    16434    Users    TABLE     �  CREATE TABLE public."Users" (
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    last_name character varying(150),
    avatar character varying(150),
    sex integer,
    password character varying(255) NOT NULL,
    salt character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    role_id integer,
    "createdAt" date,
    "updatedAt" date,
    name character varying(150)
);
    DROP TABLE public."Users";
       public         tbadmin    false    204            �            1259    16441    users_rights_id_seq    SEQUENCE     |   CREATE SEQUENCE public.users_rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.users_rights_id_seq;
       public       tbadmin    false            �            1259    16443    Users_rights    TABLE     n  CREATE TABLE public."Users_rights" (
    id integer DEFAULT nextval('public.users_rights_id_seq'::regclass) NOT NULL,
    user_id integer NOT NULL,
    "create" boolean DEFAULT false,
    read boolean DEFAULT false,
    update_action boolean DEFAULT false,
    delete_action boolean DEFAULT false,
    "createdAt" date,
    "updatedAt" date,
    right_id integer
);
 "   DROP TABLE public."Users_rights";
       public         tbadmin    false    206            �            1259    16606    Views    TABLE     �   CREATE TABLE public."Views" (
    id integer NOT NULL,
    "updatedAt" date,
    "createdAt" date,
    product_id integer NOT NULL,
    product_type public.product_type NOT NULL,
    ip character varying(50) NOT NULL
);
    DROP TABLE public."Views";
       public         tbadmin    false    674            �            1259    16451    cities_id_seq    SEQUENCE     v   CREATE SEQUENCE public.cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.cities_id_seq;
       public       tbadmin    false            �            1259    16455    firm_moderations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.firm_moderations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.firm_moderations_id_seq;
       public       tbadmin    false            �            1259    16457    firm_pages_id_seq    SEQUENCE     z   CREATE SEQUENCE public.firm_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.firm_pages_id_seq;
       public       tbadmin    false            �            1259    16459    firm_services_id_seq    SEQUENCE     }   CREATE SEQUENCE public.firm_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.firm_services_id_seq;
       public       tbadmin    false            �            1259    16461    firm_user_pages_id_seq    SEQUENCE        CREATE SEQUENCE public.firm_user_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.firm_user_pages_id_seq;
       public       tbadmin    false            �            1259    16463    firm_user_services_id_seq    SEQUENCE     �   CREATE SEQUENCE public.firm_user_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.firm_user_services_id_seq;
       public       tbadmin    false            �            1259    16465    firms_id_seq    SEQUENCE     u   CREATE SEQUENCE public.firms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.firms_id_seq;
       public       tbadmin    false            �            1259    16467    messages_id_seq    SEQUENCE     x   CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.messages_id_seq;
       public       tbadmin    false            �            1259    16469    Сities    TABLE     �   CREATE TABLE public."Сities" (
    id integer DEFAULT nextval('public.cities_id_seq'::regclass) NOT NULL,
    name character varying(150) NOT NULL,
    country_id integer NOT NULL,
    "createdAt" date,
    "updatedAt" date
);
    DROP TABLE public."Сities";
       public         tbadmin    false    208                      0    16404    Access_tokens 
   TABLE DATA               t   COPY public."Access_tokens" (id, user_id, ip, token, exp_date, "createdAt", "updatedAt", refresh_token) FROM stdin;
    public       tbadmin    false    197   �       $          0    16517 
   Categories 
   TABLE DATA               ^   COPY public."Categories" (id, "createdAt", "updatedAt", name, "position", public) FROM stdin;
    public       tbadmin    false    219   )�       *          0    16559    Category_links 
   TABLE DATA               o   COPY public."Category_links" (id, "updatedAt", "createdAt", product_id, category_id, product_type) FROM stdin;
    public       tbadmin    false    225   f�       +          0    16588    Comments 
   TABLE DATA               y   COPY public."Comments" (id, "updatedAt", "createdAt", user_id, title, description, product_id, product_type) FROM stdin;
    public       tbadmin    false    226   ��                 0    16410 	   Countries 
   TABLE DATA               I   COPY public."Countries" (id, name, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    199   ��       .          0    16611    Emotions 
   TABLE DATA               i   COPY public."Emotions" (id, "updatedAt", "createdAt", product_id, product_type, ip, emotion) FROM stdin;
    public       tbadmin    false    229   ��       #          0    16508    Files 
   TABLE DATA               �   COPY public."Files" (id, folder, is_used, section, item_id, user_id, type, main, "order", title, file, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    218   �       &          0    16525 	   Hash_tags 
   TABLE DATA               I   COPY public."Hash_tags" (id, "createdAt", "updatedAt", name) FROM stdin;
    public       tbadmin    false    221   �                 0    16416    Images 
   TABLE DATA               �   COPY public."Images" (id, title, user_id, country_id, city_id, image, slug, "createdAt", "updatedAt", description, public, category_id) FROM stdin;
    public       tbadmin    false    201   #�                 0    16425    Rights_blocks 
   TABLE DATA               T   COPY public."Rights_blocks" (id, title, slug, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    203   ��       )          0    16535 	   Tag_links 
   TABLE DATA               e   COPY public."Tag_links" (id, tag_id, product_id, "createdAt", "updatedAt", product_type) FROM stdin;
    public       tbadmin    false    224   �                 0    16434    Users 
   TABLE DATA               }   COPY public."Users" (id, last_name, avatar, sex, password, salt, email, role_id, "createdAt", "updatedAt", name) FROM stdin;
    public       tbadmin    false    205   F�                 0    16443    Users_rights 
   TABLE DATA               �   COPY public."Users_rights" (id, user_id, "create", read, update_action, delete_action, "createdAt", "updatedAt", right_id) FROM stdin;
    public       tbadmin    false    207   ΄       -          0    16606    Views 
   TABLE DATA               ]   COPY public."Views" (id, "updatedAt", "createdAt", product_id, product_type, ip) FROM stdin;
    public       tbadmin    false    228   �       "          0    16469    Сities 
   TABLE DATA               S   COPY public."Сities" (id, name, country_id, "createdAt", "updatedAt") FROM stdin;
    public       tbadmin    false    217   �       7           0    0    access_token_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.access_token_id_seq', 1, true);
            public       tbadmin    false    196            8           0    0    categories_id    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.categories_id', 2, true);
            public       tbadmin    false    220            9           0    0    cities_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.cities_id_seq', 8, true);
            public       tbadmin    false    208            :           0    0    comment_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.comment_id_seq', 1, false);
            public       tbadmin    false    227            ;           0    0    countries_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.countries_id_seq', 3, true);
            public       tbadmin    false    198            <           0    0    files_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.files_id_seq', 10, true);
            public       tbadmin    false    209            =           0    0    firm_moderations_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.firm_moderations_id_seq', 1, false);
            public       tbadmin    false    210            >           0    0    firm_pages_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.firm_pages_id_seq', 1, false);
            public       tbadmin    false    211            ?           0    0    firm_services_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.firm_services_id_seq', 1, false);
            public       tbadmin    false    212            @           0    0    firm_user_pages_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.firm_user_pages_id_seq', 1, false);
            public       tbadmin    false    213            A           0    0    firm_user_services_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.firm_user_services_id_seq', 1, false);
            public       tbadmin    false    214            B           0    0    firms_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.firms_id_seq', 7, true);
            public       tbadmin    false    215            C           0    0    hash_tag_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.hash_tag_id_seq', 3, true);
            public       tbadmin    false    222            D           0    0    messages_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.messages_id_seq', 1, false);
            public       tbadmin    false    216            E           0    0    rights_blocks_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.rights_blocks_id_seq', 3, true);
            public       tbadmin    false    202            F           0    0    tag_links_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.tag_links_id_seq', 2, true);
            public       tbadmin    false    223            G           0    0    tours_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.tours_id_seq', 10, true);
            public       tbadmin    false    200            H           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 1, true);
            public       tbadmin    false    204            I           0    0    users_rights_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.users_rights_id_seq', 3, true);
            public       tbadmin    false    206            u           2606    16521    Categories Categories_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT "Categories_pkey" PRIMARY KEY (id);
 H   ALTER TABLE ONLY public."Categories" DROP CONSTRAINT "Categories_pkey";
       public         tbadmin    false    219            �           2606    16563 "   Category_links Category_links_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public."Category_links"
    ADD CONSTRAINT "Category_links_pkey" PRIMARY KEY (id);
 P   ALTER TABLE ONLY public."Category_links" DROP CONSTRAINT "Category_links_pkey";
       public         tbadmin    false    225            �           2606    16595    Comments Comments_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT "Comments_pkey" PRIMARY KEY (id);
 D   ALTER TABLE ONLY public."Comments" DROP CONSTRAINT "Comments_pkey";
       public         tbadmin    false    226            �           2606    16618    Emotions Emotions_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public."Emotions"
    ADD CONSTRAINT "Emotions_pkey" PRIMARY KEY (id);
 D   ALTER TABLE ONLY public."Emotions" DROP CONSTRAINT "Emotions_pkey";
       public         tbadmin    false    229            s           2606    16512    Files Files_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Files"
    ADD CONSTRAINT "Files_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Files" DROP CONSTRAINT "Files_pkey";
       public         tbadmin    false    218            z           2606    16529    Hash_tags Hash_tags_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."Hash_tags"
    ADD CONSTRAINT "Hash_tags_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public."Hash_tags" DROP CONSTRAINT "Hash_tags_pkey";
       public         tbadmin    false    221                       2606    16542    Tag_links Tag_links_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."Tag_links"
    ADD CONSTRAINT "Tag_links_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public."Tag_links" DROP CONSTRAINT "Tag_links_pkey";
       public         tbadmin    false    224            w           2606    16552    Categories categories_name 
   CONSTRAINT     W   ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT categories_name UNIQUE (name);
 F   ALTER TABLE ONLY public."Categories" DROP CONSTRAINT categories_name;
       public         tbadmin    false    219            }           2606    16550    Hash_tags hash_tags_name 
   CONSTRAINT     U   ALTER TABLE ONLY public."Hash_tags"
    ADD CONSTRAINT hash_tags_name UNIQUE (name);
 D   ALTER TABLE ONLY public."Hash_tags" DROP CONSTRAINT hash_tags_name;
       public         tbadmin    false    221            a           2606    16474    Countries pk_c_id 
   CONSTRAINT     Q   ALTER TABLE ONLY public."Countries"
    ADD CONSTRAINT pk_c_id PRIMARY KEY (id);
 =   ALTER TABLE ONLY public."Countries" DROP CONSTRAINT pk_c_id;
       public         tbadmin    false    199            q           2606    16476    Сities pk_cities_id 
   CONSTRAINT     T   ALTER TABLE ONLY public."Сities"
    ADD CONSTRAINT pk_cities_id PRIMARY KEY (id);
 @   ALTER TABLE ONLY public."Сities" DROP CONSTRAINT pk_cities_id;
       public         tbadmin    false    217            _           2606    16478    Access_tokens pk_id 
   CONSTRAINT     S   ALTER TABLE ONLY public."Access_tokens"
    ADD CONSTRAINT pk_id PRIMARY KEY (id);
 ?   ALTER TABLE ONLY public."Access_tokens" DROP CONSTRAINT pk_id;
       public         tbadmin    false    197            h           2606    16480    Rights_blocks pk_r_b_id 
   CONSTRAINT     W   ALTER TABLE ONLY public."Rights_blocks"
    ADD CONSTRAINT pk_r_b_id PRIMARY KEY (id);
 C   ALTER TABLE ONLY public."Rights_blocks" DROP CONSTRAINT pk_r_b_id;
       public         tbadmin    false    203            f           2606    16482    Images pk_tours_id 
   CONSTRAINT     R   ALTER TABLE ONLY public."Images"
    ADD CONSTRAINT pk_tours_id PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Images" DROP CONSTRAINT pk_tours_id;
       public         tbadmin    false    201            n           2606    16484    Users_rights pk_u_r_id 
   CONSTRAINT     V   ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT pk_u_r_id PRIMARY KEY (id);
 B   ALTER TABLE ONLY public."Users_rights" DROP CONSTRAINT pk_u_r_id;
       public         tbadmin    false    207            j           2606    16486    Users pk_users_id 
   CONSTRAINT     Q   ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT pk_users_id PRIMARY KEY (id);
 =   ALTER TABLE ONLY public."Users" DROP CONSTRAINT pk_users_id;
       public         tbadmin    false    205            �           2606    16610    Views views_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public."Views"
    ADD CONSTRAINT views_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."Views" DROP CONSTRAINT views_pkey;
       public         tbadmin    false    228            �           1259    16605    comment_product_index    INDEX     `   CREATE INDEX comment_product_index ON public."Comments" USING btree (product_type, product_id);
 )   DROP INDEX public.comment_product_index;
       public         tbadmin    false    226    226            �           1259    16569    fki_category_link_category_id    INDEX     a   CREATE INDEX fki_category_link_category_id ON public."Category_links" USING btree (category_id);
 1   DROP INDEX public.fki_category_link_category_id;
       public         tbadmin    false    225            �           1259    16604    fki_comment_user_id    INDEX     M   CREATE INDEX fki_comment_user_id ON public."Comments" USING btree (user_id);
 '   DROP INDEX public.fki_comment_user_id;
       public         tbadmin    false    226            o           1259    16487    fki_country_id_fk    INDEX     M   CREATE INDEX fki_country_id_fk ON public."Сities" USING btree (country_id);
 %   DROP INDEX public.fki_country_id_fk;
       public         tbadmin    false    217            b           1259    16580    fki_images_category_id    INDEX     R   CREATE INDEX fki_images_category_id ON public."Images" USING btree (category_id);
 *   DROP INDEX public.fki_images_category_id;
       public         tbadmin    false    201            c           1259    16586    fki_images_user_id    INDEX     J   CREATE INDEX fki_images_user_id ON public."Images" USING btree (user_id);
 &   DROP INDEX public.fki_images_user_id;
       public         tbadmin    false    201            k           1259    16488    fki_right_id_fk    INDEX     N   CREATE INDEX fki_right_id_fk ON public."Users_rights" USING btree (right_id);
 #   DROP INDEX public.fki_right_id_fk;
       public         tbadmin    false    207            l           1259    16489    fki_user_id_fk    INDEX     L   CREATE INDEX fki_user_id_fk ON public."Users_rights" USING btree (user_id);
 "   DROP INDEX public.fki_user_id_fk;
       public         tbadmin    false    207            {           1259    16548    hash_tags_id    INDEX     I   CREATE UNIQUE INDEX hash_tags_id ON public."Hash_tags" USING btree (id);
     DROP INDEX public.hash_tags_id;
       public         tbadmin    false    221            x           1259    16547    id    INDEX     9   CREATE INDEX id ON public."Categories" USING btree (id);
    DROP INDEX public.id;
       public         tbadmin    false    219            d           1259    16587    images_public_index    INDEX     J   CREATE INDEX images_public_index ON public."Images" USING btree (public);
 '   DROP INDEX public.images_public_index;
       public         tbadmin    false    201            �           1259    16545    tag_id    INDEX     @   CREATE INDEX tag_id ON public."Tag_links" USING btree (tag_id);
    DROP INDEX public.tag_id;
       public         tbadmin    false    224            �           2606    16570    Tag_links Tag_links_tag_id    FK CONSTRAINT     �   ALTER TABLE ONLY public."Tag_links"
    ADD CONSTRAINT "Tag_links_tag_id" FOREIGN KEY (tag_id) REFERENCES public."Hash_tags"(id);
 H   ALTER TABLE ONLY public."Tag_links" DROP CONSTRAINT "Tag_links_tag_id";
       public       tbadmin    false    221    224    2938            �           2606    16564 (   Category_links category_link_category_id    FK CONSTRAINT     �   ALTER TABLE ONLY public."Category_links"
    ADD CONSTRAINT category_link_category_id FOREIGN KEY (category_id) REFERENCES public."Categories"(id);
 T   ALTER TABLE ONLY public."Category_links" DROP CONSTRAINT category_link_category_id;
       public       tbadmin    false    225    2933    219            �           2606    16599    Comments comment_user_id    FK CONSTRAINT     {   ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT comment_user_id FOREIGN KEY (user_id) REFERENCES public."Users"(id);
 D   ALTER TABLE ONLY public."Comments" DROP CONSTRAINT comment_user_id;
       public       tbadmin    false    226    2922    205            �           2606    16490    Сities country_id_fk    FK CONSTRAINT        ALTER TABLE ONLY public."Сities"
    ADD CONSTRAINT country_id_fk FOREIGN KEY (country_id) REFERENCES public."Countries"(id);
 A   ALTER TABLE ONLY public."Сities" DROP CONSTRAINT country_id_fk;
       public       tbadmin    false    199    217    2913            �           2606    16575    Images images_category_id    FK CONSTRAINT     �   ALTER TABLE ONLY public."Images"
    ADD CONSTRAINT images_category_id FOREIGN KEY (category_id) REFERENCES public."Categories"(id);
 E   ALTER TABLE ONLY public."Images" DROP CONSTRAINT images_category_id;
       public       tbadmin    false    2933    201    219            �           2606    16581    Images images_user_id    FK CONSTRAINT     x   ALTER TABLE ONLY public."Images"
    ADD CONSTRAINT images_user_id FOREIGN KEY (user_id) REFERENCES public."Users"(id);
 A   ALTER TABLE ONLY public."Images" DROP CONSTRAINT images_user_id;
       public       tbadmin    false    2922    205    201            �           2606    16495    Users_rights right_id_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT right_id_fk FOREIGN KEY (right_id) REFERENCES public."Rights_blocks"(id);
 D   ALTER TABLE ONLY public."Users_rights" DROP CONSTRAINT right_id_fk;
       public       tbadmin    false    207    203    2920            �           2606    16500    Users_rights user_id_fk    FK CONSTRAINT     z   ALTER TABLE ONLY public."Users_rights"
    ADD CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES public."Users"(id);
 C   ALTER TABLE ONLY public."Users_rights" DROP CONSTRAINT user_id_fk;
       public       tbadmin    false    207    2922    205               ]  x���[��0 ���H�r;<��.�؋��d#�
R�����������y�o2��� 
l���GJ9�I����iR���c>��Ih?KFMc?�]�d�ŧ��,���x���gf���nq�>~���$[sO�s:&��&�����ٮ@��;z���|`! ��A0��e�1xR�ߣ��l^�.y�)QE;�\��X6ǫ�2��#��q�i��{�����z�_�e
s�`��j@NEĢ�4ߘ���͙S�t�9$LD	qtؚ�p"����(\3$���(�%�t��͠�7�huI�U�@W]���qG/}_�]2�|ڥ�3��A��$��܆��椟��C?�ѻ=�~����      $   -   x�3�420��5��50Bf�d����$��r��q�q��qqq ��
�      *      x������ � �      +      x������ � �         F   x�3估���/l������.l���3����8/,��(���
s^�T��wu"$b���� (�      .      x������ � �      #   �   x���K
1��u{�q�4I�Kx���bEA���c!)��]��|$PL��y�����0�<�i�^�8�sx?qq�"M���t�� } ��̴U S�Ҙ�z1d.��F`K(���9?�_�tff�b
WpjdV{�Cf]i��4e��:ӂi�f12.�T%�^���ך���HL����'��P�S
���&�|7�z      &   0   x�3�420��5��50Bf���q��,�I�--HI,I����� w�         �   x�u�Q
� ����t�2�z���/բp���Ӵ)ҭ�_b����	�g��e>bM[P�hh�k0ԧ����q����>=jl�f#|Cv�zX���,��г-�	b���X{)����b}eA2P��Nz��L��s�^0����f*         A   x�3估���\��L�,�-��".#�Ć;/�K��$�93�3tK�! �=... �bI      )   %   x�3�4�4�420��5��50Fff�&��r��qqq �	`         x   x�3��HL)�����4�T1JT1�PI.�(w��1uK�
J�uvʏ03LMt���4)NK�	u�������MI)H*��ru4��TIK7��ʰ�LL���3rH�M���K��4��b���� (�"\         $   x�3�4�,��?2�2�3�2�������� �dA      -      x������ � �      "   y   x�=��	�P ���B�b��c��3T�T�(�j]Ბ)�#��ݑT�Q��e`�Z�R^|C&<Qf��Ke��Ix1�F#ʅ��]���tǅ�`��������9��֏��o��)��O�K)     