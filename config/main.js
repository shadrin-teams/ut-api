const env = process.env.NODE_ENV;

module.exports = {
  'secret': 'devdacticIsAwesome',
  'baseUrl': 'http://localhost:3005', // 30000
  'publicUrl': env === 'development' ? 'http://localhost:4200' : 'http://uzbekistan-tours.ru', // 30000
  'fileUrl': env === 'development' ? 'http://localhost:3005' : 'http://uzbekistan-tours.ru:3005', // 30000
  'publicFolder': 'public/',
  'frontUrls': ['localhost:4200', 'localhost:4201', 'uzbekistan-tours.ru']
};
