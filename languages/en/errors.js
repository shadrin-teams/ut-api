module.exports = {
  'require': 'Field {field} is required',
  'number__invalid_format': 'Invalid format in field {field}',
  'email__invalid_format': 'Invalid Email format in field {field}',
  'no_exists': 'Incorrect value in field {field}'
};
