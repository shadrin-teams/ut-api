module.exports = {
  apps: [{
    'name': 'ut-api-dev',
    'script': './bin/www',
    'watch': true,
    "ignore_watch" : ["uploads", "public/files", "node_modules", ".idea", ".git"],
    /*    'out_file': 'combined.log',
        'error_file': 'combined.log',*/
    'env': {
      'NODE_ENV': 'development',
      'UPLOAD_URL': '/var/www/www-root/uzbekistan-tours/api/uploads/'
    },
    'env_production': {
      'NODE_ENV': 'production'
    },
  }]
};
