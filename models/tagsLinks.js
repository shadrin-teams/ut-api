let Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {

    return sequelize.define('Tag_links', {
            tag_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            product_type: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            product_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },

        }
    );
};
