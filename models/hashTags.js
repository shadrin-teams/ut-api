let Sequelize = require('sequelize');
let {helper} = require('api-shared');

module.exports = (sequelize, DataTypes) => {

    let hashTags = sequelize.define('Hash_tags', {
            name: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            slug: {
                type: Sequelize.STRING,
            },
            description: {
                type: Sequelize.TEXT,
            }
        }
    );

    hashTags.beforeValidate((data) => {
        if (!data.slug) {
            data.slug = helper.getSlug(data.name);
        }
    });

    return hashTags;
};
