module.exports = {
  apps: [{
    'name': 'ut-api-prod',
    'script': './bin/www',
    'watch': false,
    "ignore_watch" : ["uploads", "public/files", "node_modules"],
    "cron_restart": "0 0 * * *",
    /*    'out_file': 'combined.log',
        'error_file': 'combined.log',*/
    'env': {
      'NODE_ENV': 'development',
      'UPLOAD_URL': '/var/www/www-root/uzbekistan-tours/api/uploads/'
    },
    'env_production': {
      'NODE_ENV': 'production'
    },
  }]
};
